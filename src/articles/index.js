const app     = require('express')()
const bparser = require('body-parser')
const {
  guard,
  ownerAuthorization
} = require('./guard')

const {
  getArticles,
  getAuthorArticles,
  getSingleArticle,
  createArticle,
  updateArticle,
  publishArticle,
  deleteArticle,
} = require('./service')

app.use(bparser.json())

app.get('/', async (_, res) => {
  try {
    const articles = await getArticles()
    return res.status(200).json(articles)
  } catch (error) {
    console.warn(error.message)
    return res.status(501).json({ reason: error.message })
  }
})

app.get('/author/:id', guard, async (req, res) => {
  try {
    const id = req.params.id
    const articles = await getAuthorArticles(id)
    return res.status(200).json(articles)
  } catch (error) {
    console.warn(error.message)
    return res.status(501).json({ reason: error.message })
  }
})

app.get('/:id', async (req, res) => {
  try {
    const article = await getSingleArticle(req.params.id)
    return res.status(200).json(article)
  } catch (error) {
    console.warn(error.message)
    return res.status(501).json({ reason: error.message })
  }
})

app.post('/', guard, async (req, res) => {
  try {
    console.log('received POST create article, processing...')
    const article = await createArticle(req.user, req.body)
    console.log(`New article created ${article.title}`)
    return res.status(200).json(article)
  } catch (error) {
    console.warn(error.message)
    return res.status(501).json({ reason: error.message })
  }
})

app.put('/:id', guard, ownerAuthorization, async (req, res) => {
  try {
    const id = req.params.id
    const payload = req.body
    const article = await updateArticle({ id, payload })
    console.log(`Article updated ${article.title}`)
    return res.status(200).json(article)
  } catch (error) {
    console.warn(error.message)
    return res.status(501).json({ reason: error.message })
  }
})

app.patch('/publish/:id', guard, ownerAuthorization, async (req, res) => {
  try {
    const id = req.params.id
    const article = await publishArticle({ id })
    console.log(`Article updated ${article.title}`)
    return res.status(200).json(article)
  } catch (error) {
    console.warn(error.message)
    return res.status(501).json({ reason: error.message })
  }
})

app.delete('/:id', guard, ownerAuthorization, async (req, res) => {
  try {
    const article = await deleteArticle(req.params.id)
    console.log(`Article deleted ${article.title}`)
    return res.status(200).json(article)
  } catch (error) {
    console.warn(error.message)
    return res.status(501).json({ reason: error.message })
  }
})

module.exports = app;