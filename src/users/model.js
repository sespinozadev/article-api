const mongoose   = require('mongoose')
const validator  = require('validator')

const userSchema = new mongoose.Schema({
  name: String,
  surname: String,
  email:  {
    type: String,
    required: true,
    unique: true,
    lowercase: true,
    validate: value => {
      if (!validator.isEmail(value)) {
        throw new Error('Invalid Email address')
      }
    }
  },
  phone: String,
  webpage: String,
  password: {
    type: String,
    required: true,
    minLength: 7
  }, 
  role: String,
  tokens: [{
    token: {
        type: String,
        required: true
    }
  }],
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now }
})

const User = mongoose.model('User', userSchema)

module.exports = User

// credit from: https://github.com/fatukunda/user-registration-api