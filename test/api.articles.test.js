const { expect } = require('chai');
const { app }    = require('../src/api/server');
const request    = require('supertest')

describe('Articles Controller', () => {
  describe('GET', () => {
    it('should get articles', done => {
      request(app)
        .get('/articles')
        .expect(200)
        .expect(res => {
          expect(res.body).to.match(/GET \/articles/)
        })
        .end(done)
    });
  });

  describe.skip('PATCH', () => {
    it('should update lines', done => {
      request(app)
        .patch('/lines/1')
        .expect(200)
        .expect(res => {
          expect(res.body).to.match(/PATCH \/lines\/1/)
        })
        .end(done)
    });
  });
  

  describe.skip('DELETE', () => {
    it('should delete lines', done => {
      request(app)
        .delete('/lines/1/')
        .expect(200)
        .expect(res => {
          expect(res.body).to.match(/DELETE \/lines\/1/)
        })
        .end(done)
    });
  });

  describe.skip('POST /:id/freeze', () => {
    it('should freeze lines', done => {
      request(app)
        .post('/lines/1/freeze')
        .expect(200)
        .expect(res => {
          expect(res.body).to.match(/POST \/lines\/1\/freeze/)
        })
        .end(done)
    });
  });
});