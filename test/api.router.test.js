const { expect } = require('chai');
const { app }    = require('../src/api/server');
const request    = require('supertest')

describe('Router', () => {
  it('should handle not found', done => {
    request(app)
      .get('/not-found')
      .expect(404)
      .expect('Content-Type', /json/)
      .expect(res => {
        expect(res.body).to.have.key('reason', 'message');
        expect(res.body).to.include({ reason: 'Not found' });
      })
      .end(done)
  });
});